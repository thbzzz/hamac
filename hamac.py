#!/usr/bin/env python3

from os.path import basename, dirname, join, splitext
from sys import argv
from textwrap import wrap
from tabulate import tabulate

import numpy as np
from PIL import Image

# hama beads colors and names
from hama import hama

img_path = argv[1]
img_beads_path = join(dirname(img_path),
                      splitext(basename(img_path))[0] + "-hama.png")


def closest(colors, color):
    colors = np.array(colors)
    color = np.array(color)
    distances = np.sqrt(np.sum((colors - color)**2, axis=1))
    index_of_smallest = np.where(distances == np.amin(distances))
    smallest_distance = colors[index_of_smallest]
    return smallest_distance


# Open the image
im = Image.open(img_path)

# Get the width and height of the image
width, height = im.size

# Create an image with same dimensions
im_beads = Image.new("RGB", im.size, color="white")
pixels_beads = im_beads.load()

# Dict to store needed beads and how many of them
beads_count = {}

# Loop through every pixel in the image
for x in range(width):
    for y in range(height):
        # Get the pixel color at (x, y)
        pixel = im.getpixel((x, y))

        # Get the closest bead
        closest_bead = tuple(
            closest([[int(x, 16) for x in wrap(color, 2)]
                     for color in hama.keys()], pixel[:3])[0])

        # Draw the corresponding pixel on the new image
        pixels_beads[x, y] = closest_bead

        # Increment the count for this bead
        if closest_bead in beads_count.keys():
            beads_count[closest_bead] += 1
        else:
            beads_count[closest_bead] = 0

# Save new image
im_beads.save(img_beads_path)

# Close the images
im.close()
im_beads.close()

# Dict acting as a pretty version of beads_count
beads_needed = {}

# Prints needed beads and how many of them you need, sorted decreasingly
for bead_color in sorted(beads_count,
                         key=lambda k: beads_count[k],
                         reverse=True):
    bead = hama["".join([hex(c)[2:].upper().zfill(2) for c in bead_color])]
    beads_needed[bead[0]] = (bead[2], beads_count[bead_color])
    # print(f"{bead[0]}: {bead[2]} ({beads_count[bead_color]})")

print(
    tabulate([[beads_needed[k][1] for k in beads_needed]],
             [f"{k} ({beads_needed[k][0]})" for k in beads_needed]))
