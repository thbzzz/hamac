# hamac

<div align="center">

  <img src="img/hamac-16x.png">
</div>

## Installation

```console
git clone https://gitlab.com/thbzzz/hamac.git
cd hamac
pip install -r requirements.txt
```

## Usage

For example you want to reproduce this nice pixel art of Kisame.

<div align="center">

  <img src="img/kisame.png">
</div>

Just execute `hamac.py` with your image's path as argument. It will output the beads you need and how many of them.

```console
./hamac.py img/kisame.png 
  97 (Glacier pastel)    71 (Gris foncé)    12 (Marron)    64 (Perle)    96 (Lilas pastel)    29 (Lie-de-vin)    27 (Beige)    17 (Gris)    31 (Turquoise)    76 (Marron glacé)    07 (Violet)    82 (Prune)    75 (Café frappé)    21 (Marron clair)    60 (Marron "nounours")    20 (Caramel)
---------------------  -----------------  -------------  ------------  -------------------  -----------------  ------------  -----------  ----------------  -------------------  -------------  ------------  ------------------  -------------------  ------------------------  --------------
                 1798                776            395           134                   90                 85            54           47                38                   37             26            26                  22                   10                         8               2
```

A clone of the given image is also created using the beads' colors, so you can compare it and eventually make changes in the color-bead assocations.

<div align="center">

  <img src="img/kisame-hama.png">
</div>

This image will be saved by the side of the given one, with its name suffixed with `-hama`.